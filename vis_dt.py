import dash
import dash_core_components as dcc
import dash_html_components as html

app = dash.Dash()

colors = {
    'background': '#111111',
    'text': '#7FDBFF'
}

app.layout = html.Div(children=[
    html.H1(children='Hello Dash',
        style={
            'textAlign': 'center',
            'color': colors['text'],
            'paper_bgcolor': colors['background'],
        }),
    html.H2(children='''
        Dash: A web application framework for Python.
    ''', style={
        'textAlign': 'center',
        'color': colors['text'],
        'paper_bgcolor': colors['background'],
    }),

    dcc.Graph(
        id='example-graph',
        figure={
            'data': [
                {'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'Slava'},
                {'x': [1, 2, 3], 'y': [2, 4, 5], 'type': 'point', 'name': u'Леха'},
                {'x': [1, 2, 3], 'y': [2, 7, 5], 'type': 'bar', 'name': 'Alicia'},
            ],
            'layout': {
                'plot_bgcolor': colors['background'],
                'paper_bgcolor': colors['background'],
                'font': {
                    'color': colors['text']
                }
            }
        }
    )
])

if __name__ == '__main__':
    app.run_server(debug=True)
